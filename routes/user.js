const router = require('express').Router();
const userController = require('../controllers/user');

// get all Users
router.get('/', async (req, res) => {
	try {
		const users = await userController.getAllUsers();
		res.json(users);
	} catch (err) {
		console.log(err.message);
		res.json('Error')
	}
})

// get user by id
// post a user
// put a user
// delete a user by id

module.exports = router;