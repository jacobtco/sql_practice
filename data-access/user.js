const Pool = require('pg').Pool;
const pool = new Pool({
	user: 'me',
	password: 'password',
	port: '5432',
	host: 'localhost',
	database: 'api',
});

module.exports.getUsers = async (searchParams) => {
	const searchQuery = searchParams !== undefined 
		? `WHERE ${searchParams}`
		: '';
	const res = await pool.query(`SELECT * FROM users ${searchQuery}`);
	return res.rows;
} 