const express = require('express');
const app = express();
const port = 4000;

const queries = require('./queries.js');
const userRouter = require('./routes/user');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get('/', (req, res) => {
	res.json({ info: false})
})

app.use('/users', userRouter);

app.listen(port, () => {
	console.log(`App running on port ${port}`);
});