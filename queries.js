const Pool = require('pg').Pool;
const pool = new Pool({
	user: 'me',
	password: 'password',
	host: 'localhost',
	database: 'api',
	port: 5432
})

module.exports.getUsers = async (req, res) => {
	const users = await pool.query('SELECT * FROM users ORDER BY name ASC');
	res.json(users.rows);
}