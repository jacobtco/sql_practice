const userDataAccess = require('../data-access/user');

module.exports.getAllUsers = async () => {
	return await userDataAccess.getUsers();
}